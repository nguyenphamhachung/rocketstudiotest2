using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject box;
    GameObject[,] boxes = new GameObject[100, 100];
    private void Start()
    {
        for(int i = 0; i < 100; i++)
        {
            for(int j = 0; j < 100; j++)
            {
                boxes[i, j] = Instantiate(box,transform);
                boxes[i, j].transform.localPosition = new Vector3(2*i, 0, 2*j);
            }
        }
    }
}
