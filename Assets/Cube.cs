using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    
    void Start()
    {
        int randomStart = Random.Range(0, 2);
        if(randomStart == 0)
        {
            MoveUp();
        }
        else
        {
            MoveDown();
        }
    }
    void MoveUp()
    {
        transform.DOMoveY(transform.position.y + 1f, 2f).SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                transform.DOMoveY(transform.position.y - 2f, 4f).SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    transform.DOMoveY(transform.position.y + 1f, 2f).SetEase(Ease.Linear)
                    .OnComplete(() =>
                    {
                        MoveUp();
                    });
                });
            });
    }
    void MoveDown()
    {
        transform.DOMoveY(transform.position.y - 1f, 2f).SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                transform.DOMoveY(transform.position.y + 2f, 4f).SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    transform.DOMoveY(transform.position.y - 1f, 2f).SetEase(Ease.Linear)
                    .OnComplete(() =>
                    {
                        MoveDown();
                    });
                });
            });
    }
    enum Dir
    {
        Up,
        Down,
    }
}
